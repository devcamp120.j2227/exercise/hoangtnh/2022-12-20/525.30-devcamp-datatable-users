import {
    FETCH_USERS_PENDING,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_ERROR,
    SELECT_USER_HANDLER,
    USERS_PAGINATION_CHANGE
} from "../constants/user.contants";
const initialState = {
    users: [],
    pending: false,
    error: null,
    userData: null,
    totalUser: 0,
    currentPage: 1,
}

export default function userReducer(state = initialState , action){
    switch (action.type) {
        case FETCH_USERS_PENDING:
            state.pending = true
            break;
        case  FETCH_USERS_SUCCESS:
            state.totalUser = action.totalUser
            state.users = action.data;            
            state.pending = false
            break;
        case SELECT_USER_HANDLER:
            state.userData = action.data;
            break;
        case USERS_PAGINATION_CHANGE:
            state.currentPage = action.page;
            break;
        default:
            break;
    }
    return {...state}
}