import {
    FETCH_USERS_PENDING,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_ERROR,
    SELECT_USER_HANDLER,
    USERS_PAGINATION_CHANGE
} from "../constants/user.contants";
export const selectUser = (data) =>{
    return{
        type: SELECT_USER_HANDLER,
        data: data
    }
}
export const changePagination = (page) =>{
    return {
        type: USERS_PAGINATION_CHANGE,
        page: page
    }
}
export const fetchUser = (size, currentPage) =>{
    return async (dispatch) =>{
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };
          await dispatch({
            type: FETCH_USERS_PENDING
          });
          try {
            const response = await fetch("http://203.171.20.210:8080/devcamp-register-java-api/user-list-pagination", requestOptions);
            const dataTotalUser = await response.json();

            const responseLimitUser = await fetch("http://203.171.20.210:8080/devcamp-register-java-api/user-list-pagination?page=" + currentPage  + "&size=" + (size-1), requestOptions);
            const data =  await responseLimitUser.json()
            return dispatch({
                type: FETCH_USERS_SUCCESS,
                totalUser: dataTotalUser.length,
                data: data
            })
          } catch(error){
            return dispatch({
                type: FETCH_USERS_ERROR,
                error: error
            })
          }
    }
 }